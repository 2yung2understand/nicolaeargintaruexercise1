# Exercise 1

## Birthday Cake Candles
You are in charge of the cake for a child's birthday. You have decided the cake will have one candle for each year of their total age. They will only be able to blow out the tallest of the candles. Count how many candles are tallest. 

### Example
$`candles=[4,4,3,1]`$  
The maximum height candles are $`4`$ units high. There are $`2`$ of them, so return $`2`$.

### Function Description
Write the function *birthdayCakeCandles* in any programming language you know.

#### Input Parameters
*birthdayCakeCandles* has the following input parameter:
1. int $`candles[n]`$: the candle heights

#### Returns
1. int: the number of candles that are tallest

#### Input Format
The first line contains a single integer, $`n`$, the size of $`candles[]`$.
The second line contains $`n`$ space-separated integers, where each integer $`i`$ describes the height of $`candles[i]`$.

#### Constrains
1. $`1 <= n <= 10^3`$
2. $`1 <= candles[i] <= 10^3`$

#### Sample Input
> 4  
> 3 2 1 3

#### Sample Output
> 2
