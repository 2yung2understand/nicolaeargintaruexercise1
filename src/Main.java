import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println(birthdayCakeCandles());
    }

    public static int birthdayCakeCandles() {

        Scanner input = new Scanner(System.in);
        //n constraint input and check if its in between 1 and 10^3
        int n = input.nextInt();
        if (n < 1 || n > 1000) {
            System.exit(0);
        }

        //candles[n] input and checking if candles[i] is in between 1 and 10^3
        //int max is a reference point to count the highest candles for the return
        int max = 0;
        int candles[] = new int[n];
        for (int i = 0; i < n; i++) {
            candles[i] = input.nextInt();
            if (candles[i] < 1 || candles[i] > 1000)
                System.exit(0);
            if (max < candles[i])
                max = candles[i];
        }

        //counting the highest candles
        int result = 0;
        for (int i = 0; i < n; i++) {
            if (max == candles[i]) {
                result++;
            }
        }

        return result;

    }

}
